## Welcome to ValueAdd Challenge
Setup

First, clone the repository.

Then, install project dependencies.

Don't forget to send us your code after you complete the challenge. 
You can do it by creating your own repository - pssst... make it private, we don't want anyone to see it ;) and then share your repository with `przemyslaw.leczycki@valueadd.pl` and `dominik.gasior@valueadd.pl`
 
Below you'll find an exercise list

If you ready to check your solution just use:
`test-run.sh`

# Exercise 1
Fill the source of the class: `Exercise1`

Your task is to split the chocolate bar of given dimension n x m into small squares. Each square is of size 1x1 and unbreakable. Implement a function that will return minimum number of breaks needed.

For example if you are given a chocolate bar of size 2 x 1 you can split it to single squares in just one break, but for size 3 x 1 you must do two breaks.

If input data is invalid you should return 0 (as in no breaks are needed if we do not have any chocolate to split). Input will always be a non-negative integer.

# Exercise 2
Fill the source of the class: `Exercise2`

Implement body of the method parse
your method must return the corresponding linked list, constructed from instances of the Node,

for example, given the following string representation of a linked list:

`"1 -> 2 -> 3 -> NULL"`

your method should return:

`new Node(1, new Node(2, new Node(3)))`

# Exercise 3
It is located under the directory: `Exercise 3`

**Problem:**

Our customer gathers information on people.
They would like us to implement a migration mechanism 
which will be able to flexibly parse and covert any kind of data (for example: XML/JSON/CSV coming from files or other resources - HTTP)
and store them in a database.

**Description:**

We are given an XML file - `DataSource/people.xml` (coming from some unknown source) which contains data about people.
(Note that the scheme of provided data may differ as we are going to have different source of data provided for the system).
Our system must be able to handle and validate the following properties (all of them are required to migrate a person):
- first name: `string, max. 100 characters`
- last name: `string, max. 200 characters`
- email: `simple email validation`
- gender: `allowed values: "m", "f"` ("m" stands for male, "f" stands for female)

If more properties are provided, omit them - **parse only these four**!
If any of these properties is missing, log that the validation does not pass somewhere
(for example to a file for the sake of simplicity)

As for storing the parsed data - there are two required ways of storing the data:
- database
- JSON file (commit it to the repository)

You don't need to use a real database engine like MySQL or something like that (but you can if you prefer).
You can store it in some kind of in-memory database implementation.
It's not necessary to persist data - each program run should have a clear state.

**Rules:**

You can use whatever tools/libraries you like. The only goal is to achieve what our customer wants 
and make the system as much extendible as possible - think of the customer's needs, they may be really changeable.
Use your imagination what can change or what features will be added soon in the project.
The system should be able to quite easily change database engines, logging mechanism, parsing different data source, etc.
It should also be possible to store for example JSON or XML in a database as string - make everything flexible.
We don't impose a lot of requirements on you, the main thing is **easy extendibility** of the system's components.
In addition, it will be highly appreciated if you cover your code with unit tests.

*Good Luck*
