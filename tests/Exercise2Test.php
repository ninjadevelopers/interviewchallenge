<?php

declare(strict_types=1);

use Exercises\Exercise2;
use PHPUnit\Framework\TestCase;

final class Exercise2Test extends TestCase
{
    public function testParsing()
    {
        $this->assertEquals(new Node(1, new Node(2, new Node(3))), Exercise2::parse("1 -> 2 -> 3 -> NULL"));
        $this->assertEquals(
            new Node(0, new Node(1, new Node(4, new Node(9, new Node(16))))),
            Exercise2::parse("0 -> 1 -> 4 -> 9 -> 16 -> NULL")
        );
        $this->assertNull(Exercise2::parse("NULL"));
    }
}
