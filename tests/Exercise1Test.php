<?php

declare(strict_types=1);

use Exercises\Exercise1;
use PHPUnit\Framework\TestCase;

final class Exercise1Test extends TestCase
{
    public function testBreakingChocolate()
    {
        $this->assertEquals(Exercise1::breakChocolate(5, 5), 24);
        $this->assertEquals(Exercise1::breakChocolate(1, 1), 0);
        $this->assertEquals(Exercise1::breakChocolate(-1, 1), 0);
        $this->assertEquals(Exercise1::breakChocolate(-1, -1), 0);
    }
}
