<?php

declare(strict_types=1);

namespace Exercises;

use Node;

/**
 * Implement body of the method parse
 * your method must return the corresponding linked list, constructed from instances of the Node
 * for example, given the following string representation of a linked list:
 *
 * "1 -> 2 -> 3 -> NULL"
 *
 * your method should return
 *
 * new Node(1, new Node(2, new Node(3)))
 */
final class Exercise2
{
    public static function parse(string $pattern): ?Node
    {
        // TODO insert your code here
    }
}
