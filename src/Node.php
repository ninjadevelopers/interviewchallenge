<?php

declare(strict_types=1);

final class Node
{
    private int $data;
    private ?Node $next;

    public function __construct(int $data, Node $next = null)
    {
        $this->data = $data;
        $this->next = $next;
    }
}
